describe('Testirati checkbox Prikazi zavrsene kvarove na stranici za prikaz kvarov u jednoj zgradi  ', () => {
    beforeEach(() => {
        cy.loginZavrsna('predSkup@gmail.com', 'Bar5slova')

        cy.url().should('include', '/pocetna')
    });

    describe('Positive test', () => {
        it('testiranje checkboxa', () => {
            cy.get('.nav > :nth-child(1) > .nav-link').should('contain', 'predSkup@gmail.com')
        cy.get('#zgradaStanuje > tbody > tr > :nth-child(4) > a').click()
        cy.url().should('include', '/obavestenja')
        cy.get(':nth-child(4) > .nav-link').click()
        cy.url().should('include', '/kvarovi')
        cy.contains('pogledaj').click()
        cy.contains('zavrsi kvar').click()
        cy.get(':nth-child(4) > .nav-link').click()
        cy.get('label > .ng-untouched').click().check().should('be.checked')
        cy.get('app-kvarovi.ng-star-inserted').should('contain','KVAR JE ZAVRSEN')
        });

    });

});