describe('Testirati postavljanje vlasnika, predsednika i dodavanje stanara u stan ', () => {
    beforeEach(() => {
        cy.loginZavrsna('admin@gmail.com', 'Bar5slova')
        cy.pregledstanavlasnikapredsednika()



    });

    describe('Positive test', () => {
        it('dodavanje vlasnika ', () => {
            cy.get(':nth-child(1) > .col-md-6 > :nth-child(2) > .btn').should('be.visible')
            cy.contains('Postavi za vlasnika').click()
            cy.get('.toast-success').should('have.text', ' Uspesno ste postavili vlasnika! ')
            cy.get('.col-md-10 > a').click()
            cy.get('#stan > tbody > .table-secondary > .col-md-9 > a').click()
            cy.get('.col-md-10').should('contain','Marko Markovic')
        })

        it('brisanje vlasnika ', () => {
            cy.get('#ukloniVlasnika').click()
            cy.get('.toast-success').should('have.text', ' Uspesno ste uklonili vlasnika! ')
            cy.get('.col-md-12').should('have.text','Vlasnik: Nema vlasnika')
        })
        it('brisanje i dodavanje predsednika ', () => {
            cy.contains('Postavi za predsednika').should('be.disabled')
       
        
        })

        it('brisanje i dodavanje stanara ', () => {
            cy.get('table tr').should('have.length', 9)
            cy.get(':nth-child(1) > .col-md-6 > :nth-child(3) > .btn').click()
            cy.get('.toast-message').should('contain',' Uspesno ste uklonili vlasnika! ')
            cy.get(':nth-child(3) > .col-md-5 > :nth-child(2) > .btn').click()
       
        
        })

      
    })
})

// Nisam znao za proveru tabele pre i posle dodavanja i brisanja , trebalo je proveriti broj u tabeli pre i posle dodavanja kao asertaciju