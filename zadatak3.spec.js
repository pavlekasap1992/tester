describe('Testirati postavljanje/izbor i izmenu odgovornog lica na kvaru ', () => {                             // Primer promene lozinke sa jednim pozitivnim i jednim negativnim testom
    beforeEach(() => {
        cy.loginZavrsna('predSkup@gmail.com', 'Bar5slova')

        cy.url().should('include', '/pocetna')
        cy.dodavanjeKvarova()

    });
    describe('Positive test', () => {
        it('postavaljanje odgovornog lica od strane Predsednika', () => {

            cy.url().should('include', '/kvar')
            cy.get('#odgovorno_lice').should('contain', 'Gospodin Predsednik')



        });

        it('izmena odgovornog lica na postojecem kvaru od strane Predsednika', () => {
            cy.get(':nth-child(4) > .nav-link').click()
            cy.get('app-kvarovi').should('contain', 'Gospodin Predsednik')
            cy.contains('pogledaj').click()
            cy.get('.izmeni_odgovornog > .operacije').click()
            cy.get('#button_3').click()
            cy.get('#odgovorno_lice').should('contain', 'Marko Markovic')
            cy.get(':nth-child(1) > .toast-message').should('have.text',' Odgovorno lice uspesno izmenjeno ')
        });
    });

});