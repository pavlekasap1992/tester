describe('Testirati dropdown na stranici Dodeljeni kvarovi ', () => {
    beforeEach(() => {
        cy.loginZavrsna('predSkup@gmail.com', 'Bar5slova')

        cy.url().should('include', '/pocetna')
    });

    describe('Positive test', () => {
        it('testiranje dropdowna 1:1', () => {
            cy.get('.nav > :nth-child(1) > .nav-link').should('contain', 'predSkup@gmail.com')
            cy.get(':nth-child(2) > .nav-link').click()
            cy.get('.pogledaj_38 > .operacije').click()
            cy.contains('Kvarovi').click()
            //cy.get('.operacije lower-impact-text').should('have.text','pogledaj').click()
            
            cy.get('.mr-auto > :nth-child(2) > .nav-link').click()
            cy.get('label > .ng-untouched').click()
            cy.get('#prikaz').select('1').should('have.value','1')

        });

        it('testiranje dropdowna 2:2', () => {
            cy.get('.nav > :nth-child(1) > .nav-link').should('contain', 'predSkup@gmail.com')
            cy.get(':nth-child(2) > .nav-link').click()
            cy.get('.pogledaj_38 > .operacije').click()
            cy.contains('Kvarovi').click()
            //cy.get('.operacije lower-impact-text').should('have.text','pogledaj').click()
            
            cy.get('.mr-auto > :nth-child(2) > .nav-link').click()
            cy.get('label > .ng-untouched').click()
            cy.get('#prikaz').select('2').should('have.value','2')

        });
        it('testiranje dropdowna 10:10', () => {
            cy.get('.nav > :nth-child(1) > .nav-link').should('contain', 'predSkup@gmail.com')
            cy.get(':nth-child(2) > .nav-link').click()
            cy.get('.pogledaj_38 > .operacije').click()
            cy.contains('Kvarovi').click()
            //cy.get('.operacije lower-impact-text').should('have.text','pogledaj').click()
            
            cy.get('.mr-auto > :nth-child(2) > .nav-link').click()
            cy.get('label > .ng-untouched').click()
            cy.get('#prikaz').select('10').should('have.value','10')

        });
        it.only('testiranje dropdowna 25:25', () => {
            cy.get('.nav > :nth-child(1) > .nav-link').should('contain', 'predSkup@gmail.com')
            cy.get(':nth-child(2) > .nav-link').click()
            cy.get('.pogledaj_38 > .operacije').click()
            cy.contains('Kvarovi').click()
            //cy.get('.operacije lower-impact-text').should('have.text','pogledaj').click()
            
            cy.get('.mr-auto > :nth-child(2) > .nav-link').click()
            cy.get('label > .ng-untouched').click()
            cy.get('#prikaz').select('25').should('have.value','25')

        });

        it('testiranje dropdowna 50:50', () => {
            cy.get('.nav > :nth-child(1) > .nav-link').should('contain', 'predSkup@gmail.com')
            cy.get(':nth-child(2) > .nav-link').click()
            cy.get('.pogledaj_38 > .operacije').click()
            cy.contains('Kvarovi').click()
            //cy.get('.operacije lower-impact-text').should('have.text','pogledaj').click()
            
            cy.get('.mr-auto > :nth-child(2) > .nav-link').click()
            cy.get('label > .ng-untouched').click()
            cy.get('#prikaz').select('50').should('have.value','50')

        });

    });
});

